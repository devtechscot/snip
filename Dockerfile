FROM rust:1.26 AS development

WORKDIR /code
COPY Makefile /code
COPY Cargo.toml /code

RUN make deps

FROM development AS build

RUN make build
