dshell:
	@docker-compose run --service-ports --rm rust bash

deps:
	@cargo fetch

deps.update:
	@cargo update

build:
	@cargo build

test:
	@cargo test

run:
	@cargo run
