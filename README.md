# Snip

A URI-Shortener written in #Rust

## Developing

If you're using Docker, then please be advised that you should be inside a `dshell` when following all further instructions.

```shell
make dshell
```

### Fetching Dependencies

```shell
make deps
```

If you've modified / added a new dependency:

```shell
make deps.update
```

### Tests

```shell
make test
```

### Building

```shell
make build
```
